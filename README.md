# **Description**
This repository simulates a football championship, where the participating teams are objects to which we can assign a name, score, matches played, goals scored, conceded goals and goal diffence.
Additionally, we can print the championship table ordered by score and make sum or subtraction of scores.


## **Defining the participants**
 First, run **championship.py**.
 Use ```participant()``` to define a new participant.

### **Syntax**
```participant(name, score, matches, goals, conceded_goals, difference)```

### **Parameter values**
| Parameter | Description |
|-----------|-------------|
| name      | A string with the name of the participant |
| score     | Int type representing the participant's score |
| goals     | Int type representing the number of goals scored |
| conceded_goals| Int type representing the number of conceded goals |
| difference| Int type representing the difference between goals scored and conceded |

## **Printing a championship table**
To print a table ordered by score, use the function ```table()``` and put as parameter a list with the participants. This will return a table with the positions and all information of the participants.