class participant:
	def __init__(self, name, score, matches, goals, conceded_goals, difference):
		self.name = name
		self.score = score
		self.matches = matches
		self.goals = goals
		self.conceded = conceded_goals
		self.difference = difference

	def __repr__(self):
		return f'Name: {self.name}  Score: {self.score}  Matches: {self.matches}  Goals: {self.goals}  Conceded goals: {self.conceded}  Goal difference: {self.difference}'

	def __gt__(self, other):
		return self.score > other.score

	def __add__(self,other):
		return self.score + other.score

	def __sub__(self,other):
		return self.score - other.score

def table(participants):
	"""Receives a list of championship participants and returns a table ordered by scores"""
	pos = 1
	participants.sort(reverse=True)
	for obj in participants:
		print(f'{pos}° - ' + str(obj))
		pos += 1